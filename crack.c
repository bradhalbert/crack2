#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    char *pass = md5(guess, strlen(guess));
    if( strcmp(hash, pass) == 0)
    {
        free(pass);
        return 1;
    }
    else
    {
        free(pass);
        return 0;
    }
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    int size = 50;
    char **pwds = (char **)malloc(size * sizeof(char *));
    
    char str[40];
    int i = 0;
    FILE *fp = fopen(filename, "r");
    while(fscanf(fp, "%s", str) != EOF)
    {
        if(i == size)
        {
            size += 10;
            char **newarr = (char **)realloc(pwds, size * sizeof(char *));
            if (newarr != NULL) pwds = newarr;
            else
            {
                printf("Realloc failed\n");
                exit(1);
            }
        }
        char *newstr = (char *)malloc(strlen(str) + 1 * sizeof(char));
        strcpy(newstr, str);
        pwds[i] = newstr;
        
        i++;
    }
    fclose(fp);
    return pwds;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile("hashes.txt");

    // Read the dictionary file into an array of strings
    char **dict = readfile("rockyou100.txt");
    //For each hash, try every entry in the dictionary.
    int count;
    for(int hash = 0; hashes[hash] != NULL; hash++)
    {
        for(int i = 0; dict[i] != NULL; i++)
        {
            int try = tryguess(hashes[hash], dict[i]);
            if(try == 1)
            {
                printf("%s %s\n", hashes[hash], dict[i]);
            }
        }
        count = hash;
    }
    
    //freeing all the data
    for(int i = 0; hashes[i] != NULL; i++)
    {
        free(hashes[i]);
    }
    for(int i = 0; dict[i] != NULL; i++)
    {
        free(dict[i]);
    }
    
    //free pointers
    free(hashes);
    free(dict);
}
